import 'dart:collection';
import 'dart:convert';
import 'dart:typed_data';

import 'package:typed_data/typed_buffers.dart';
import 'type_bytes.dart' as bytes;

/// Basis for a higher level BSON encoder
class ChunkedBsonEncoder {
  final Uint8Buffer _buffer;
  int _index = 4;
  final _subdocumentQueue = Queue<int>();

  /// [initialLength] of the underlying buffer.
  ChunkedBsonEncoder([int initialLength = 150])
      : _buffer = Uint8Buffer(initialLength);

  /// Lets the buffer grow if you have a good guess
  /// how much space you need.
  void grow(int rate) {
    _buffer.length += rate;
  }

  void _growIfTooSmall(int growth) {
    if ((_index + growth) >= _buffer.length) {
      grow(150 + growth);
    }
  }

  void _setTypeAndKey(int type, String key) {
    const typeByte = 1;
    final encKey = utf8.encode(key + bytes.nullTerminator);
    // key length + type
    _growIfTooSmall(encKey.length + typeByte);
    _buffer[_index] = type;
    _index += typeByte;
    _buffer.setAll(_index, encKey);
    _index += encKey.length;
  }

  void addString(String key, String value) {
    _setTypeAndKey(0x02, key);
    final encValue = utf8.encode(value + bytes.nullTerminator);

    _growIfTooSmall(encValue.length + bytes.i32);
    _buffer.buffer
        .asByteData()
        .setUint32(_index, encValue.length, Endian.little);
    _index += bytes.i32;
    _buffer.setAll(_index, encValue);
    _index += encValue.length;
  }

  void addDouble(String key, double value) {
    _setTypeAndKey(0x01, key);

    _growIfTooSmall(bytes.i64);
    _buffer.buffer.asByteData().setFloat64(_index, value, Endian.little);
    _index += bytes.i64;
  }

  void addBoolean(String key, bool value) {
    _setTypeAndKey(0x08, key);

    _growIfTooSmall(1);
    _buffer[_index] = value ? 0x01 : 0x00;
    _index++;
  }

  void addNullValue(String key) {
    _setTypeAndKey(0x08, key);
  }

  void addObjectId(String key, Uint8List value) {
    assert(value.length == 12);
    _setTypeAndKey(0x07, key);

    const objectIdBytes = 12;
    _growIfTooSmall(objectIdBytes);
    _buffer.setAll(_index, value);
    _index += objectIdBytes;
  }

  void addBinaryData(String key, Uint8List value, [int subtype = 0x00]) {
    _setTypeAndKey(0x05, key);
    const lengthAndTypeBytes = bytes.i32 + 1;
    _growIfTooSmall(value.length + lengthAndTypeBytes);

    _buffer.buffer.asByteData().setInt32(_index, value.length, Endian.little);
    _index += bytes.i32;
    // binary subtype
    _buffer[_index] = subtype;
    _index++;
    _buffer.setAll(_index, value);
    _index += value.length;
  }

  /// UTC datetime as milliseconds since Unix epoch
  void addUtc(String key, int value) {
    _setTypeAndKey(0x09, key);
    _growIfTooSmall(bytes.i64);

    _buffer.buffer.asByteData().setInt64(_index, value, Endian.little);
    _index += bytes.i64;
  }

  void addInt64(String key, int value) {
    _setTypeAndKey(0x12, key);
    _growIfTooSmall(bytes.i64);
    final i64 = ByteData(bytes.i64)..setInt64(0, value, Endian.little);
    _buffer.setAll(_index, i64.buffer.asUint8List());
    _index += bytes.i64;
  }

  void addUint64(String key, int value) {
    _setTypeAndKey(0x12, key);
    _growIfTooSmall(bytes.i64);
    _buffer.buffer.asByteData().setInt64(_index, value, Endian.little);
    _index += bytes.i64;
  }

  void addInt32(String key, int value) {
    assert(value.bitLength <= 32);
    _setTypeAndKey(0x10, key);
    _growIfTooSmall(bytes.i32);
    _buffer.buffer.asByteData().setInt32(_index, value, Endian.little);
    _index += bytes.i32;
  }

  void addMinKey(String key) {
    _setTypeAndKey(0xFF, key);
  }

  void addMaxKey(String key) {
    _setTypeAndKey(0x7F, key);
  }

  /// Copies [doc] into the underlying buffer.
  /// Use this for already existing documents that
  /// should become a subdocument.
  void addDocument(String key, Uint8List doc) {
    assert(doc.buffer.asUint32List()[0] == doc.length,
        'Document hasn\'t expected length');
    assert(doc.last == 0x00, 'Document is not null terminated');
    _setTypeAndKey(0x03, key);
    _growIfTooSmall(doc.length);
    _buffer.setAll(_index, doc);
    _index += doc.length;
  }

  /// Starts a new subdocument. All further added values
  /// will be added to the subdocument. Subdocuments can be nested.
  /// All subdocuments must be ended with [documentEnd].
  void documentStart(String key) {
    _setTypeAndKey(0x03, key);
    _growIfTooSmall(bytes.i32);
    _subdocumentQueue.add(_index);
    _index += bytes.i32;
  }

  /// Ends a subdocument. [startIndex] is the starting index
  /// of the subdocument.
  void documentEnd() {
    final documentStartIndex = _subdocumentQueue.removeLast();

    /// real doc length + doc terminator
    final docLength = _index - documentStartIndex + 2;
    _buffer.buffer
        .asByteData()
        .setInt32(documentStartIndex, docLength, Endian.little);
    _growIfTooSmall(1);
    _buffer[_index] = 0x00;
    _index++;
  }

  /// Copies [array] into the underlying buffer.
  /// Use this for already existing arrays that
  /// should become a subdocument.
  void addArray(String key, Uint8List array) {
    assert(array.buffer.asUint32List()[0] == array.length,
        'Document hasn\'t expected length');
    assert(array.last == 0x00, 'Array is not null terminated');
    _setTypeAndKey(0x04, key);
    _growIfTooSmall(array.length);
    _buffer.setAll(0, array);
    _index += array.length;
  }

  /// Start a new array. Arrays are just a special kind of
  /// subdocument. The keys of the elements of an array
  /// are their indices as String like '0'. The indices of an array must
  /// consecutive.
  void arrayStart(String key) {
    _setTypeAndKey(0x04, key);
    _growIfTooSmall(bytes.i32);
    _subdocumentQueue.add(_index);
    _index += bytes.i32;
  }

  /// Ends the array. [startIndex] is the starting index of the array.
  /// Has the same effect as [documentEnd] and is only there for symmetry.
  void arrayEnd() {
    documentEnd();
  }

  /// Returns the finished BSON encoded document.
  /// The [ChunkedBsonEncoder] mustn't be used anymore afterwards.
  UnmodifiableUint8ListView build() {
    if (_subdocumentQueue.isNotEmpty) {
      final end = _subdocumentQueue.length;
      for (var i = 0; i < end; i++) {
        documentEnd();
      }
    }
    // terminate document
    _buffer[_index] = 0x00;
    _buffer.buffer.asByteData().setUint32(0, _index + 1, Endian.little);
    return UnmodifiableUint8ListView(
        Uint8List.view(_buffer.buffer, 0, _index + 1));
  }
}
