import 'dart:convert';
import 'dart:typed_data';
import 'package:bson/object_id.dart';
import 'package:bson/src/binary_subtype.dart';

import 'bson_types.dart' as types;
import 'type_bytes.dart' as bytes;

/// Basis for a higher level Converter
class ChunkedBsonDecoder {
  final UnmodifiableUint8ListView _bson;
  var _index = 4;
  ChunkedBsonDecoder(this._bson);

  String _extractKey() {
    final indexOfTerminator = _bson.indexOf(0x00, _index);
    final keyView =
        Uint8List.view(_bson.buffer, _index, indexOfTerminator - _index);
    final key = utf8.decode(keyView);
    _index = indexOfTerminator + 1;
    return key;
  }

  /// Extracts the next MapEntry from BSON document.
  /// Returns sybtypes of [DocumentStatus].
  /// Throws [Formatexception] when MapEntry has an
  /// unfamiliar BSON type.
  ///
  /// Calling it after the last [EndOfDocument] results
  /// in a [RangeError].
  MapEntry<String, dynamic> nextEntry() {
    final type = _bson[_index++];
    if (type == types.terminator) {
      return null;
    }
    final key = _extractKey();
    switch (type) {
      case types.double:
        final value =
            _bson.buffer.asByteData().getFloat64(_index, Endian.little);
        _index += bytes.double;
        return MapEntry(key, value);
      case types.string:
        final length =
            _bson.buffer.asByteData().getInt32(_index, Endian.little);
        _index += bytes.i32;
        final view = Uint8List.view(_bson.buffer, _index, length - 1);
        final value = utf8.decode(view);
        _index += length;
        return MapEntry(key, value);
      case types.subdocument:
        _index += bytes.i32;
        return MapEntry(key, <String, dynamic>{});
      case types.array:
        _index += bytes.i32;
        return MapEntry(key, []);
      case types.binary:
        final length =
            _bson.buffer.asByteData().getInt32(_index, Endian.little);
        _index += types.i32;
        final subtype = _bson[_index++];
        final data = Uint8List.view(_bson.buffer, _index, length);
        _index += length;
        final binaryData = BinaryData.custom(data, subtype);
        return MapEntry(key, binaryData);
      case types.objectId:
        final objectId = Uint8List.view(_bson.buffer, _index, 12);
        _index += 12;
        final unmodifiableObjectId =
            ObjectId.fromEncoded(UnmodifiableUint8ListView(objectId));
        return MapEntry(key, unmodifiableObjectId);
      case types.boolean:
        final value = _bson[_index] == 0x01;
        _index++;
        return MapEntry(key, value);
      case types.utc:
        final value =
            _bson.buffer.asByteData().getUint64(_index, Endian.little);
        _index += bytes.i64;
        return MapEntry(key, DateTime.fromMillisecondsSinceEpoch(value));
      case types.nullValue:
        return MapEntry(key, null);
      case types.i32:
        final value = _bson.buffer.asByteData().getInt32(_index, Endian.little);
        _index += bytes.i32;
        return MapEntry(key, value);
      case types.i64:
        final value = _bson.buffer.asByteData().getInt64(_index, Endian.little);
        _index += bytes.i64;
        return MapEntry(key, value);
    }
    throw FormatException(
        "Invalid Bson type '${type.toRadixString(16)}' at position $_index");
  }

  Iterable<MapEntry<String, dynamic>> get iterable sync* {
    yield nextEntry();
  }
}
