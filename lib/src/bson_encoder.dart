import 'dart:collection';
import 'dart:convert';

import 'package:bson/object_id.dart';
import 'package:bson/src/binary_subtype.dart';
import 'min_max_key.dart';

import 'chunked_bson_encoder.dart';
import 'bson_document.dart';

class BsonEncoder extends Converter<Map<String, Object>, BsonDocument> {
  ChunkedBsonEncoder _builder;
  Iterator<MapEntry<String, Object>> _currentDocument;
  Queue<Iterator<MapEntry<String, Object>>> _documentQueue;

  void _convertElement(String key, Object value) {
    if (value is String) {
      _builder.addString(key, value);
    } else if (value is int) {
      if (value.bitLength <= 32) {
        _builder.addInt32(key, value);
      } else {
        if (value.sign == 1) {
          _builder.addUint64(key, value);
        } else {
          _builder.addInt64(key, value);
        }
      }
    } else if (value is double) {
      _builder.addDouble(key, value);
    } else if (value is BsonDocument) {
      _builder.addDocument(key, value.bytes);
    } else if (value is Map<String, Object>) {
      _builder.documentStart(key);
      _documentQueue.add(_currentDocument);
      _currentDocument = value.entries.iterator;
    } else if (value is List<Object>) {
      _builder.arrayStart(key);
      _documentQueue.add(_currentDocument);
      _currentDocument = _MapEntryIterator(value.iterator);
    } else if (value is DateTime) {
      _builder.addUtc(key, value.millisecondsSinceEpoch);
    } else if (value is BinaryData) {
      _builder.addBinaryData(key, value.bytes, value.subtype);
    } else if (value == minKey) {
      _builder.addMinKey(key);
    } else if (value == maxKey) {
      _builder.addMaxKey(key);
    } else if (value is ObjectId) {
      _builder.addObjectId(key, value.bytes);
    } else if (value is bool) {
      _builder.addBoolean(key, value);
    } else {
      throw ArgumentError.value(value, key, 'not supportet type');
    }
  }

  @override
  BsonDocument convert(Map<String, Object> input) {
    _builder = ChunkedBsonEncoder();
    _documentQueue = Queue();
    _currentDocument = input.entries.iterator;
    for (;;) {
      final hasMoreElements = _currentDocument.moveNext();
      if (!hasMoreElements) {
        if (_documentQueue.isEmpty) {
          return BsonDocument(_builder.build());
        }
        if (_documentQueue.isNotEmpty) {
          _currentDocument = _documentQueue.removeLast();
          _builder.documentEnd();
          continue;
        }
      }
      final entry = _currentDocument.current;
      _convertElement(entry.key, entry.value);
    }
  }
}

final bsonEncoder = BsonEncoder();

class _MapEntryIterator<T> extends Iterator<MapEntry<String, T>> {
  final Iterator<T> _inner;
  int _count = -1;
  int get count => _count;
  MapEntry<String, T> _current;

  _MapEntryIterator(this._inner);

  @override
  bool moveNext() {
    final hasNext = _inner.moveNext();
    if (!hasNext) {
      return false;
    }
    _count++;
    _current = MapEntry(_count.toString(), _inner.current);
    return true;
  }

  @override
  MapEntry<String, T> get current => _current;
}
