import 'dart:collection';
import 'dart:convert';
import 'bson_document.dart';

import 'chunked_bson_decoder.dart';

class BsonDecoder extends Converter<BsonDocument, Map<String, dynamic>> {
  ChunkedBsonDecoder _decoder;
  Object _destination;

  Queue<Object> _documentQueue;
  MapEntry<String, dynamic> _currentEntry;

  void _addEntriesList() {
    List<dynamic> dest = _destination;
    for (;;) {
      if (_currentEntry == null) {
        _handleDocumentEnd();
        return;
      }
      if (_currentEntry.value is List) {
        dest.add(_currentEntry.value);
        _documentQueue.add(dest);
        _destination = _currentEntry.value;
        dest = _currentEntry.value;
        _currentEntry = _decoder.nextEntry();
        continue;
      }
      if (_currentEntry.value is Map) {
        dest.add(_currentEntry.value);
        _documentQueue.add(dest);
        _destination = _currentEntry.value;
        _currentEntry = _decoder.nextEntry();
        _addEntriesMap();
        return;
      }
      dest.add(_currentEntry.value);
      _currentEntry = _decoder.nextEntry();
    }
  }

  void _addEntriesMap() {
    Map<String, dynamic> dest = _destination;
    for (;;) {
      if (_currentEntry == null) {
        _handleDocumentEnd();
        return;
      }
      if (_currentEntry.value is Map) {
        dest[_currentEntry.key] = _currentEntry.value;
        _documentQueue.add(dest);
        _destination = _currentEntry.value;
        dest = _currentEntry.value;
        _currentEntry = _decoder.nextEntry();
        continue;
      }
      if (_currentEntry.value is List) {
        dest[_currentEntry.key] = _currentEntry.value;
        _documentQueue.add(dest);
        _destination = _currentEntry.value;
        _currentEntry = _decoder.nextEntry();
        _addEntriesList();
        return;
      }
      dest[_currentEntry.key] = _currentEntry.value;
      _currentEntry = _decoder.nextEntry();
    }
  }

  void _handleDocumentEnd() {
    if (_documentQueue.isEmpty) {
      return;
    }
    _destination = _documentQueue.removeLast();
    _currentEntry = _decoder.nextEntry();
    if (_destination is Map) {
      _addEntriesMap();
      return;
    }
    if (_destination is List) {
      _addEntriesList();
      return;
    }
  }

  @override
  Map<String, dynamic> convert(BsonDocument bson) {
    _decoder = ChunkedBsonDecoder(bson.bytes);
    _destination = <String, Object>{};
    _documentQueue = Queue();
    _currentEntry = _decoder.nextEntry();
    _addEntriesMap();
    return _destination;
  }
}

final bsonDecoder = BsonDecoder();
