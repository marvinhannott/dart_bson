import 'dart:convert';

import 'package:bson/bson.dart';
import 'package:bson/src/bson_document.dart';

class BsonCodec extends Codec<Map<String, Object>, BsonDocument> {
  @override
  final encoder = BsonEncoder();
  @override
  final decoder = BsonDecoder();
}

final bsonCodec = BsonCodec();
