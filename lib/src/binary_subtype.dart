import 'dart:typed_data';

/// Wrapper type to signal to the encoder that this isn't an
/// array, but encoded binary data. [subtype] describes
/// the type of binary data.
class BinaryData {
  final Uint8List bytes;
  final int subtype;
  const BinaryData.generic(this.bytes) : subtype = 0x00;
  const BinaryData.uuid(this.bytes) : subtype = 0x04;
  const BinaryData.md5(this.bytes) : subtype = 0x04;
  const BinaryData.custom(this.bytes, this.subtype)
      : assert(subtype >= 0x80 && subtype <= 0xff);
}
