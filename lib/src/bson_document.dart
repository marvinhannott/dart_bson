import 'dart:typed_data';

/// Wrapper type to signal to the encoder that this isn't
/// an array, but an already encoded BSON document,
/// which should become a subdocument.
class BsonDocument {
  final Uint8List bytes;
  const BsonDocument(this.bytes);

  Uint8List toJson() => bytes;
}
