import 'dart:math';
import 'dart:typed_data';

/// MongoDB ObjectId
class ObjectId {
  static final _generate = _ObjectIdWrapper();

  /// Byte representation of encoded ObjectId
  final UnmodifiableUint8ListView bytes;

  /// Generates a new ObjectId
  ObjectId() : bytes = _generate();

  /// Just wraps an already encoded ObjectId
  ObjectId.fromEncoded(Uint8List bytes)
      : bytes = UnmodifiableUint8ListView(bytes),
        assert(bytes.length == 12);

  /// Generates an encoded ObjectId from its hexadecimal representation.
  factory ObjectId.fromHexString(String objectId) {
    assert(objectId.length == 24);
    final encoded = Uint8List(12);
    for (var i = 0, j = 0; i < 12; i++, j += 2) {
      encoded[i] = int.tryParse(objectId.substring(j, j + 2), radix: 16);
    }
    return ObjectId.fromEncoded(encoded);
  }

  /// Hexadecimal representation of ObjectId
  String asHexString() {
    return bytes.map((e) => e.toRadixString(16)).join();
  }

  @override
  String toString() {
    return 'ObjectId("${asHexString()}")';
  }

  String toJson() {
    return asHexString();
  }

  /// Get the timestamp value (in senconds since the Unix epoch)
  /// of [objectId]
  int get timestamp {
    final buffer = bytes.buffer.asByteData();
    return buffer.getUint32(0, Endian.big);
  }
}

/// Wraps the random value and counter
UnmodifiableUint8ListView Function() _ObjectIdWrapper() {
  final rand = Random.secure();
  var counter = rand.nextInt(pow(2, 24));
  final randValue = rand.nextInt(pow(2, 32));
  final randValueRest = rand.nextInt(256);
  return () {
    final timestamp = DateTime.now().millisecondsSinceEpoch ~/ 1000;
    final objectId = ByteData(12)
      ..setUint32(0, timestamp, Endian.big)
      ..setUint32(4, randValue, Endian.little)
      ..setUint8(8, randValueRest)
      // counter in big endian
      ..setUint8(9, (counter >> 16) & 0xff)
      ..setUint8(10, (counter >> 8) & 0xff)
      ..setUint8(11, (counter & 0xff));
    counter = (counter + 1).toUnsigned(24);
    return UnmodifiableUint8ListView(objectId.buffer.asUint8List());
  };
}
