import 'dart:convert';
import 'dart:typed_data';

import 'package:benchmark_harness/benchmark_harness.dart';
import 'package:bson/bson.dart';
import 'package:bson/object_id.dart';
import 'package:bson/src/binary_subtype.dart';

final document = {
  '_id': ObjectId(),
  'bin': BinaryData.generic(UnmodifiableUint8ListView(Uint8List(32))),
  'ioerg': 23,
  'wawaef': 'waefwe',
  'jiserg': {
    'ijowaef': [
      12,
      'ewf',
      89,
      {'awefwef': 'waefjoij'}
    ]
  },
  'ijiowef': [23, 'wef', 23],
  'jioewf': true
};

class EncodingBenchmark extends BenchmarkBase {
  EncodingBenchmark() : super('EncodingBenchmark');
  final encoder = BsonEncoder();

  @override
  void run() {
    encoder.convert(document);
  }
}

class DecodingBenchmark extends BenchmarkBase {
  final decoder = BsonDecoder();
  final encodedDocument = BsonEncoder().convert(document);

  DecodingBenchmark() : super('DecodingBenchmark');
  @override
  void run() {
    decoder.convert(encodedDocument);
  }
}

class JsonEncodeCompareBenchmark extends BenchmarkBase {
  JsonEncodeCompareBenchmark() : super('JsonCompareBenchmark');

  @override
  void run() {
    json.encode(document);
  }
}

class JsonDecodeCompareBenchmark extends BenchmarkBase {
  JsonDecodeCompareBenchmark() : super('JsonDecodeCompareBenchmark');
  final encoded = json.encode(document);
  @override
  void run() {
    json.decode(encoded);
  }
}

void main() {
  EncodingBenchmark().report();
  DecodingBenchmark().report();
  JsonEncodeCompareBenchmark().report();
  JsonDecodeCompareBenchmark().report();
}
