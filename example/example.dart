import 'dart:typed_data';

import 'package:bson/object_id.dart';
import 'package:bson/src/binary_subtype.dart';
import 'package:bson/src/bson_codec.dart';

void main() {
  final binData = Uint8List.fromList([1, 2, 3]);
  final doc = {
    '_id': ObjectId(),
    'key': 'value',
    'subDocument': {
      'array': [1, 2, 3],
      'binary': BinaryData.generic(binData)
    }
  };
  final codec = BsonCodec();
  final encoded = codec.encode(doc);
  print(encoded.bytes);

  final decoded = codec.decode(encoded);
  print(decoded);
}
